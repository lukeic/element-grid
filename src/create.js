const sketch = require('sketch');
const UI = require('sketch/ui');
const Rectangle = require('sketch/dom').Rectangle;

export default function () {
	const document = sketch.getSelectedDocument();
	const selectedLayers = document.selectedLayers;

	UI.getInputFromUser(
		'Grid Size (rows columns padding)',
		{
			initialValue: '4 4 20'
		},
		(err, input) => {
			if (err && !err.includes('user canceled input')) {
				UI.alert('An error occurred. Please report it to the plugin creator', err);
				return;
			}

			if (selectedLayers.length !== 1) {
				UI.message('Only one layer must be selected.');
				return;
			}

			const rowsColumnsAndPadding = input
				.trim()
				.split(' ');

			const numRows = parseInt(rowsColumnsAndPadding[0]);
			const numCols = parseInt(rowsColumnsAndPadding[1]);
			const padding = parseFloat(rowsColumnsAndPadding[2]);
			if (!numRows || isNaN(numRows) || !numCols || isNaN(numCols) || isNaN(padding)) {
				UI.message('A value for rows, columns, and padding is required.');
				return;
			}

			const element = selectedLayers.layers[0];
			if (!element) {
				UI.message(`No element was found in the currently selected layer. This shouldn't happen, so submit a bug report at the plugin's repository.`);
				return;
			}

			const { width: originalWidth, height: originalHeight } = element.frame;

			const renderElementsForRow = (rowNumber) => {
				for (let i = 0; i < numCols; i++) {
					const xOffset = (originalWidth * i) + (padding * i);
					const yOffset = (originalHeight * rowNumber) + (padding * rowNumber);
					// Lunacy seems to have a problem with frame.offset(). Creating a new Rectangle works however.
					const dupe = element.duplicate();
					dupe.frame = new Rectangle(element.frame.x + xOffset, element.frame.y + yOffset, originalWidth, originalHeight);
				}
			};

			for (let row = 0; row < numRows; row++) {
				renderElementsForRow(row);
			}

			element.remove();
		}
	);
}
