<div align="center">
    <img src="https://gitlab.com/lukeic/element-grid/-/raw/master/assets/icon.png" width="80">
</div>

# Element Grid
Duplicates an element and arranges it in a grid.

## How to Use
1. Select a single layer
2. Run the plugin
3. Input values for rows, columns and padding. E.g. `2 2 20`
